# queryall_fs

## Description:
    A tool to import configuration files from IBM FlashSystem Storage for viewing existing configuration and creating scripts.

## Required modules:
    openpyxl

## Usage Instructions:
    Export the .xml configuration backup from a FlashSystem cluster using the Settings-->Support menu in the FS GUI.
    Create a data directory in the root directory:  root-->data-->[customer_name]
    Create two folders inside the custoemr directory:  input, output
    Copy the XML files into the input directory.
    Update config.py with the name of the customer as used in the file structure shown above.  (Temporary - plan to eventually have a menu or front-end interface)
    The script will create an XLSX workbook for each XML file located in the customer-->input directory
